BIN_DIR ?= /usr/local/bin
INTERPRETER ?= tclsh

install:
	install -m 0755 owh.tcl $(BIN_DIR)/owh
	sed -i '1 s/tclsh/$(INTERPRETER)/' $(BIN_DIR)/owh

uninstall:
	-rm $(BIN_DIR)/owh

.PHONY: install uninstall
