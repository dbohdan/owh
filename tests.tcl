#! /usr/bin/env tclsh

package require tcltest

set argv [lassign $argv tcl]
if {$tcl eq {}} { set tcl [info nameofexecutable] }
::tcltest::configure {*}$argv


proc owh args {
    upvar 1 tcl tcl

    set dir [file dirname [info script]]
    exec {*}$tcl [file join $dir owh.tcl] {*}$args
}


tcltest::test basic-1.1 {} -body {
    owh {1 { print $0 }} << 1\n2\n3\n
} -result 1\n2\n3

tcltest::test basic-1.2 {} -body {
    owh {1 { print $0 }} << 1\n2\n3
} -result 1\n2\n3

tcltest::test basic-1.3 {} -body {
    owh {0 { print $0 }} << 1\n2\n3\n
} -result {}


tcltest::test begin-end-1.1 {} -body {
    owh {
        BEGIN           { set max 0   }
        { $NF > $max }  { set max $NF }
        END             { puts $max   }
    } << "foo bar\nbaz baz baz\n\na b c d e\n0 1 2"
} -result 5


tcltest::test default-1.1 {} -body {
    owh {
        { $1 == 5 } {}
        { $1 == 7 } {}
        { [regexp xyz $1] } {}
        DEFAULT { print $2 }
    } << "3 a\n5 b\n7 c\n9 d\nxyz e\n"
} -result "a\nd"

tcltest::test default-1.2 {} -body {
    owh {
        { $1 == 5 } {}
        { $1 == 7 } {}
        { [regexp xyz $1] } {}
        ELSE { print $2 }
    } << "3 a\n5 b\n7 c\n9 d\nxyz e\n"
} -result "a\nd"


tcltest::test FS-1.1 {} -body {
    owh {true { print [set $NR] }} << "1 2 3\n4 5 6\n7 8 9\n"
} -result 1\n5\n9

tcltest::test FS-1.2 {} -body {
    owh {
        BEGIN { set FS 2 }
        yes { print $1 }
    } << "1 2 3\n4 5 6\n7 8 9\n"
} -result "1 \n4 5 6\n7 8 9"


tcltest::test OFS-1.1 {} -body {
    owh {
        BEGIN { set OFS % }
        1 { print $1 $2 $3 }
    } << "1 2 3\n4 5 6\n7 8 9\n"
} -result 1%2%3\n4%5%6\n7%8%9


tcltest::test input-file-1.1 {} -body {
    set path [tcltest::makeFile "1 2 3\n4 5 6\n7 8 9" input]

    owh { true { print $3 } } $path
} -result 3\n6\n9


# Exit with a nonzero status if there are failed tests.
set failed [expr {$tcltest::numTests(Failed) > 0}]

tcltest::cleanupTests
if {$failed} {
    exit 1
}
