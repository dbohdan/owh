#! /usr/bin/env tclsh

proc awksplit {0 {split default}} {
    if {$split eq "default"} {
        set t [lsearch -all -inline -not [split $0] {}]
    } else {
        set t [split $0 $split]
    }

    set result [dict create]
    dict set result NF [llength $t]

    set i 1
    foreach field $t {
        dict set result $i $field
        incr i
    }

    return $result
}

proc print args {
    upvar 1 OFS OFS

    if {[catch {
        puts [join $args $OFS]
    }]} {
        exit 0
    }
}

proc usage {} {
    set engine [expr {[catch {
        proc foo {} {} {}
        rename foo {}
    }] ? {Tcl} : {Jim}}]
    if {[info exists ::tcl_platform(engine)]} {
        set engine $::tcl_platform(engine)
    }
    set version [info patchlevel]

    puts "usage: owh patterns \[file\]
    performs action (in $engine $version) for each line (\$0) from stdin
    owh: Ousterhout - Welch - Hobbs, to name a few"
}

proc main args {
    switch [llength $args] {
        1 { set input stdin }
        2 { set input [open [lindex $args 1] r] }
        default { usage; exit -1 }
    }

    set FS default
    set OFS { }

    # Process $args.
    set __begin {}
    set __default {}
    set __end {}
    set __patterns {}
    # Do not use [dict unset] to retain the key order in Jim Tcl 0.79 and
    # earlier.
    dict for {__expr __body} [lindex $args 0] {
        switch -- $__expr {
            BEGIN { set __begin $__body }
            DEFAULT -
            ELSE { set __default $__body }
            END { set __end $__body }
            default { lappend __patterns $__expr $__body }
        }
    }
    unset args

    # Prepare for the main loop.
    set NF 0
    set NR 0

    eval $__begin

    while true {
        if {[gets $input 0] == -1} break

        for {set __i 1} {$__i <= $NF} {incr __i} {
            unset $__i
        }
        unset __i

        incr NR

        set __split [awksplit $0 $FS]
        dict with __split {}
        unset __split

        set __matched 0
        dict for {__expr __body} $__patterns {
            if $__expr { eval $__body; set __matched 1 }
        }
        if {!$__matched} {
            eval $__default
        }
    }

    close $input

    set res [eval $__end]
    if {[string length $res]} {
        puts $res
    }
}

eval main $argv
